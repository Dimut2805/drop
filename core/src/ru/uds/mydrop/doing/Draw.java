package ru.uds.mydrop.doing;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Draw {
    SpriteBatch spriteBatch = new SpriteBatch();
    void draw();
}
