package ru.uds.mydrop;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ru.uds.mydrop.model.Bucket;
import ru.uds.mydrop.model.Drop;

public class Main implements ApplicationListener {
    private OrthographicCamera camera;
    private Bucket bucket;
    private List<Drop> dropList;
    private long lastDropTime;

    @Override
    public void create() {
        dropList = new ArrayList<>();
        bucket = new Bucket(10, 10, 64);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 400);
    }

    private void spawnDrop() {
        dropList.add(new Drop(0 + (int) (Math.random() * Gdx.graphics.getWidth()), Gdx.graphics.getHeight() - 64, 64));
        lastDropTime = TimeUtils.nanoTime();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        bucket.draw();
        // обработка пользовательского ввода
        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            bucket.setX((int) touchPos.x - 64 / 2);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) bucket.moveLeft();
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) bucket.moveRight();
        if (bucket.getX() < 0) bucket.setX(0);
        if (bucket.getX() > Gdx.graphics.getWidth() - 64) bucket.setX(Gdx.graphics.getWidth() - 64);
        if (TimeUtils.nanoTime() - lastDropTime > 1000000000) spawnDrop();
        Iterator<Drop> iter = dropList.iterator();
        while (iter.hasNext()) {
            Drop drop = iter.next();
            drop.setY((int) (drop.getY() - 200 * Gdx.graphics.getDeltaTime()));
            if (drop.getY() + 64 < 0) iter.remove();
            if (drop.getRectangle().overlaps(bucket.getRectangle())) {
                iter.remove();
            }
            drop.draw();
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        bucket.getBucketImage().dispose();
    }
}