package ru.uds.mydrop.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import ru.uds.mydrop.doing.Draw;

public class Drop implements Draw {
    private Texture bucketImage;
    private Rectangle rectangle;
    private int x, y;

    public Drop(int x, int y, int size) {
        bucketImage = new Texture(Gdx.files.internal("droplet.png"));
        this.x = x;
        this.y = y;
        rectangle = new Rectangle(this.x, this.y, size, size);
    }

    public void setY(int y) {
        this.y = y;
        rectangle.setY(y);
    }

    public Texture getBucketImage() {
        return bucketImage;
    }

    public int getY() {
        return y;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    @Override
    public void draw() {
        spriteBatch.begin();
        spriteBatch.draw(bucketImage, x, y);
        spriteBatch.end();
    }
}
