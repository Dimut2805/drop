package ru.uds.mydrop.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import ru.uds.mydrop.doing.Draw;

public class Bucket implements Draw {
    private Texture bucketImage;
    private int x, y;
    private Rectangle rectangle;

    public Bucket(int x, int y, int size) {
        bucketImage = new Texture(Gdx.files.internal("bucket.png"));
        this.x = x;
        this.y = y;
        rectangle = new Rectangle(this.x, this.y, size, size);
    }

    public Texture getBucketImage() {
        return bucketImage;
    }

    public void setX(int x) {
        this.x = x;
        rectangle.setX(x);
    }

    public Rectangle getRectangle() {
        return rectangle;
    }


    public int getX() {
        return x;
    }

    public void moveLeft() {
        x -= 200 * Gdx.graphics.getDeltaTime();
        rectangle.setX(x);
    }

    public void moveRight() {
        x += 200 * Gdx.graphics.getDeltaTime();
        rectangle.setX(x);
    }

    @Override
    public void draw() {
        spriteBatch.begin();
        spriteBatch.draw(bucketImage, x, y);
        spriteBatch.end();
    }
}